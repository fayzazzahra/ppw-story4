from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from story8.views import book_search, book_data


# Create your tests here.

class Story8UnitTest(TestCase):

    def test_book_search_url_is_exist(self):
        response = Client().get('/book-search/')
        self.assertEqual(response.status_code, 200)

    def test_book_search_using_func(self):
        found = resolve('/book-search/')
        self.assertEqual(found.func, book_search)

    def test_experiences_using_template(self):
        response = Client().get('/book-search/')
        self.assertTemplateUsed(response,'book-search.html')

    def test_book_data_url_is_exist(self):
        response = Client().get('/book-search/data?buku=frozen')
        self.assertEqual(response.status_code, 200)

    def test_book_data_using_func(self):
        found = resolve('/book-search/data')
        self.assertEqual(found.func, book_data)