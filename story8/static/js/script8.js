$(document).ready(function() {

    $.ajax({
        method: 'GET',
        url: '/book-search/data?buku=ajax/',
        success: function(result) {
            var res = "";
            for (let i = 0; i < result.items.length; i++) {
                res += ('<tr>' + '<td>' + (i+1) + '</td>')
                res += ('<td> <a href="' + result.items[i].volumeInfo.canonicalVolumeLink + '">' + result.items[i].volumeInfo.title + ' </a> </td>' + '<td>' + result.items[i].volumeInfo.authors + '</td>' + '<td>')

                if (result.items[i].volumeInfo.description != undefined) {
                    if (result.items[i].volumeInfo.description.length > 230) {
                        res += result.items[i].volumeInfo.description.substring(0,230) + '...' + '</td>'
                    } else {
                        res += result.items[i].volumeInfo.description + '</td>'
                    }
                } else {
                    res += "---"
                }

                if (result.items[i].volumeInfo.imageLinks != null) {
                    res += ('<td><img src=' + result.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>' + '</tr>')
                } else {
                    res += ('<td> <p> --- </p> </td>' + '</tr>')
                }
            }
            //console.log(res);
            $('#book-data').append(res);
            $('#book-data').append('</table>');
        }
    })

    var input = document.getElementById("keyword");
    input.addEventListener("keyup", function(event) {
      if (event.keyCode === 13) {
       event.preventDefault();
       document.getElementById("search").click();
      }
    })

    $("#search").click(function(){
        var buku = $("#keyword").val();
        // console.log(buku);
        // var url_books = 'https://www.googleapis.com/books/v1/volumes?q=' + buku;
        // console.log(url_books);

        $.ajax({
            method: 'GET',
            url: '/book-search/data?buku=' + buku + '/',
            success: function(result) {
                //console.log(result);
                $('#book-data').empty();

                var res = "";
                for (let i = 0; i < result.items.length; i++) {
                    res += ('<tr>' + '<td>' + (i+1) + '</td>')
                    res += ('<td> <a href="' + result.items[i].volumeInfo.canonicalVolumeLink + '">' + result.items[i].volumeInfo.title + ' </a> </td>' + '<td>' + result.items[i].volumeInfo.authors + '</td>' + '<td>')

                    if (result.items[i].volumeInfo.description != undefined) {
                        if (result.items[i].volumeInfo.description.length > 230) {
                            res += result.items[i].volumeInfo.description.substring(0,230) + '...' + '</td>'
                        } else {
                            res += result.items[i].volumeInfo.description + '</td>'
                        }
                    } else {
                        res += "---"
                    }

                    if (result.items[i].volumeInfo.imageLinks != null) {
                        res += ('<td><img src=' + result.items[i].volumeInfo.imageLinks.smallThumbnail + '></td>' + '</tr>')
                    } else {
                        res += ('<td> <p> --- </p> </td>' + '</tr>')
                    }
                }
                // console.log(res);
                $('#book-data').append(res);
                $('#book-data').append('</table>');
            }
        })
    });
});