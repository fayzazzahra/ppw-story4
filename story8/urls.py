from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('', views.book_search, name='book-search'),
    path('data', views.book_data, name='book-data')

]