from django.shortcuts import redirect, render
from django.http import HttpResponse, JsonResponse
import requests

# Create your views here.

def book_search(request):
    return render(request, 'book-search.html')

def book_data(request):
    keyword = request.GET['buku']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + keyword
    result = requests.get(url_tujuan)
    data = result.json()
    return JsonResponse(data, safe=False)