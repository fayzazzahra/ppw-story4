from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def experiences(request):
    return render(request, 'experiences.html')
