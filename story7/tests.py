from django.test import TestCase, Client
from django.urls import reverse, resolve
from story7.views import experiences
from django.http import HttpRequest

# Create your tests here.

class Story7UnitTest(TestCase):

    def test_experiences_url_is_exist(self):
        response = Client().get('/experiences/')
        self.assertEqual(response.status_code, 200)

    def test_experiences_add_using_func(self):
        found = resolve('/experiences/')
        self.assertEqual(found.func, experiences)

    def test_experiences_using_template(self):
        response = Client().get('/experiences/')
        self.assertTemplateUsed(response,'experiences.html')

    def test_experiences_views(self):
        request = HttpRequest()
        response = experiences(request)
        isi_html = response.content.decode('utf8')
        self.assertIn('Experiences', isi_html)
        self.assertIn('Current Activities', isi_html)
        self.assertIn('Achievements', isi_html)