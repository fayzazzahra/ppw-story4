$(document).ready(function() {

    $('.accordion').find('.accordion-header').click(function() {  
        $(this).next().slideToggle();
        
        // close all other menu items
        $(".accordion-content").not($(this).next()).slideUp();
        });

    $('button').click(
        function(event) {
            event.preventDefault(); // agar memprevent selector button untuk melakukan action yang default dari elemen tersebut
            var parent = $(this).closest('div').parent().parent(); //membuat suatu variable untuk akses parent dari parent dari div terdekat.
            event.stopPropagation();  // agar tidak tertimpa dengan div diatasnya
            if ($(this).hasClass('btn-move-up')) { 
                parent.insertBefore(parent.prev('div'));
                
            }
            else if ($(this).hasClass('btn-move-down')) {
                parent.insertAfter(parent.next('div'));

            }
        });
        
});