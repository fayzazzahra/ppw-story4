# Generated by Django 3.1.2 on 2020-10-16 10:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('matkul', '0003_auto_20201015_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='matkul',
            name='ruang',
            field=models.CharField(blank=True, default='Google Meet', max_length=50, verbose_name='Ruang Kelas'),
        ),
    ]
