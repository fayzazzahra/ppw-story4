from django.shortcuts import redirect, render
from django.http import HttpResponseRedirect
from .models import Matkul
from .forms import MatkulForm

# Create your views here.

def daftarkan(request):
    form = MatkulForm()
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            data_input = Matkul()
            data_input.nama = data['nama']
            data_input.dosen = data['dosen']
            data_input.sks = data['sks']
            data_input.deskripsi = data['deskripsi']
            data_input.smt = data['smt']
            data_input.ruang = data['ruang']
            data_input.save()
            current_data = Matkul.objects.all()
            return render(request, 'matkul.html', {'form':form, 'status':'success', 'data':current_data})
        else:
            current_data = Matkul.objects.all()
            return render(request, 'matkul.html', {'form':form, 'status':'failed', 'data':current_data})
    else:
        current_data = Matkul.objects.all()
        return render(request, 'matkul.html', {'form':form, 'data':current_data})

def data(request):
    if request.method == 'POST':
        matkul = Matkul.objects.all()
        Matkul.objects.filter(id=request.POST['id']).delete()
        return redirect('matkul:data')
    else:
        matkul = Matkul.objects.all()
        return render(request, 'data.html', {'matkul':matkul})

def detail(request, index):
    matkul = Matkul.objects.get(pk=index)
    return render(request, 'detail.html', {'matkul':matkul})