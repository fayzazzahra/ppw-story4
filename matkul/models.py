from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

gsl1920 = 'Gasal 2019/2020'
gnp1920 = 'Genap 2019/2020'
gsl2021 = 'Gasal 2020/2021'
gnp2021 = 'Genap 2020/2021'
SMT_CHOICES = (
    (gsl1920, 'Gasal 2019/2020'),
    (gnp1920, 'Genap 2019/2020'),
    (gsl2021, 'Gasal 2020/2021'),
    (gnp2021, 'Genap 2020/2021'),)

class Matkul(models.Model):

    nama = models.CharField('Mata Kuliah', max_length=50)
    dosen = models.CharField('Dosen Pengajar', max_length=50)
    sks = models.IntegerField('Jumlah SKS', default=1, 
        validators =[
                MinValueValidator(1),
                MaxValueValidator(6)
        ])
    deskripsi = models.TextField('Deskripsi', max_length=200)
    smt = models.CharField('Semester', choices=SMT_CHOICES, 
        default=gsl2021, max_length=200, null=True)
    ruang = models.CharField('Ruang Kelas', max_length=50, blank=True, default="Google Meet")

    def __str__(self):
        return f'{self.matkul}'

    class Meta:
        db_table = 'jadwal'