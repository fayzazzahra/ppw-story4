from django import forms
from .models import Matkul
from .models import SMT_CHOICES
from django.contrib.admin import models

class MatkulForm(forms.ModelForm):

    class Meta:
        model = Matkul
        fields = '__all__'
        
        
        widgets = {
            'nama':forms.TextInput(attrs={'class':'form-control'}),
            'dosen':forms.TextInput(attrs={'class':'form-control'}),
            'sks':forms.NumberInput(attrs={'class':'form-control','minlength': 1, 'maxlength': 6, 'required': True, 'type': 'number',}),
            'deskripsi':forms.Textarea(attrs={'class':'form-control'}),
            'smt':forms.Select(choices=SMT_CHOICES, attrs={'class':'form-control'}),
            'ruang':forms.TextInput(attrs={'class':'form-control'}),
        }