from django.contrib import admin
from . import models

# Register your models here.

@admin.register(models.Matkul)
class MatkulAdmin(admin.ModelAdmin):
    list_display = ('nama', 'dosen', 'sks', 'smt', 'ruang', 'deskripsi')
    search_fields = ('nama', 'dosen', 'sks', 'smt', 'ruang')