from django.urls import path

from . import views

app_name = 'matkul'

urlpatterns = [
    path('daftarkan/', views.daftarkan, name='daftarkan'),
    path('data/', views.data, name='data'),
    path('data/<int:index>/', views.detail, name="detail"),
]