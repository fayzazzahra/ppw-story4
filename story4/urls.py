"""story4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('story1/', include('story1.urls'), name='story1'),
    path('story3/', include('story3.urls')),
    path('matkul/', include('matkul.urls')),
    path('kegiatan/', include('story6.urls'), name='kegiatan'),
    path('experiences/', include('story7.urls'), name='experiences'),
    path('book-search/', include('story8.urls'), name='book-search'),
    path('accounts/', include('story9.urls'), name='accounts'),
    path('accounts/', include('django.contrib.auth.urls'))
]
