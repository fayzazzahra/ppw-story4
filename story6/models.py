from django.db import models

# Create your models here.

class Kegiatan(models.Model):
    kegiatan = models.CharField('Kegiatan', max_length=100,  null=True, blank=False)

    class Meta:
        db_table = 'kegiatan'

    def __str__(self):
        return self.kegiatan


class Peserta(models.Model):
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.CASCADE)
    nama = models.CharField('Daftarkan diri Anda', max_length=50, null=True, blank=False)

    class Meta:
        db_table = 'peserta'

    def __str__(self):
        return self.nama