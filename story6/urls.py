from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('daftarkan/', views.daftarkan, name='daftarkan'),
    path('data/', views.data, name='data'),
    
]