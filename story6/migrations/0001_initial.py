# Generated by Django 3.1.2 on 2020-10-24 08:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Kegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kegiatan', models.CharField(max_length=100, verbose_name='Kegiatan')),
            ],
            options={
                'db_table': 'kegiatan',
            },
        ),
        migrations.CreateModel(
            name='Peserta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=50, verbose_name='Peserta')),
                ('kegiatan', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='story6.kegiatan')),
            ],
            options={
                'db_table': 'peserta',
            },
        ),
    ]
