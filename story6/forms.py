from django import forms
from .models import Kegiatan, Peserta
from django.contrib.admin import models

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = Kegiatan
        fields = '__all__'

        widgets = {
            'kegiatan':forms.TextInput(attrs={'class':'form-control'}) }
        
class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'

        widgets = {
            'kegiatan': forms.Select(attrs={'class':'form-control'}),
            'nama':forms.TextInput(attrs={'class':'form-control'})
        }