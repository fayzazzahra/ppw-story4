from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm
from .views import daftarkan, data


# Create your tests here.

class DaftarkanKegiatanUnitTest(TestCase):

    def test_daftarkan_kegiatan_url_is_exist(self):
        response = Client().get('/kegiatan/daftarkan/')
        self.assertEqual(response.status_code, 200)

    def test_daftarkan_kegiatan_using_template(self):
        response = Client().get('/kegiatan/daftarkan/')
        self.assertTemplateUsed(response,'keg-daftarkan.html')

    def test_activity_add_using_func(self):
        found = resolve('/kegiatan/daftarkan/')
        self.assertEqual(found.func, daftarkan)

    # def test_view_form(self):
    #     response = Client().get("/kegiatan/daftarkan/")
    #     isi_html = response.content.decode("utf8")
    #     self.assertIn('Daftarkan', isi_html)
    #     self.assertIn('<div class="form-group">', isi_html)

    def test_model_name(self):
        Kegiatan.objects.create(kegiatan='kegiatan')
        kegiatan = Kegiatan.objects.get(kegiatan='kegiatan')
        self.assertEqual(str(kegiatan),'kegiatan')
        
    def test_daftarkan_kegiatan_views(self):
        Kegiatan.objects.create(kegiatan='Test')
        request = HttpRequest()
        response = daftarkan(request)
        isi_html = response.content.decode('utf8')
        self.assertIn('Daftarkan', isi_html)

    def test_model_create_kegiatan(self):
        Kegiatan.objects.create(kegiatan="storyyyuk")
        count = Kegiatan.objects.all().count()
        self.assertEquals(count, 1)

    def test_new_form_validation(self):
        new_kegiatan = Kegiatan.objects.create(kegiatan = "ngestory")
        form = PesertaForm(data = {'kegiatan':new_kegiatan, 'nama':'fayza'})
        self.assertTrue(form.is_valid())

    def test_model_name(self):
        Kegiatan.objects.create(kegiatan='kegiatan')
        kegiatan = Kegiatan.objects.get(kegiatan='kegiatan')
        self.assertEqual(str(kegiatan),'kegiatan')

    def test_model_relationship(self):
        Kegiatan.objects.create(kegiatan = "relationship")
        kegiatan_id = Kegiatan.objects.all()[0].id
        kegiatan_di_id = Kegiatan.objects.get(id=kegiatan_id)
        Peserta.objects.create(kegiatan = kegiatan_di_id, nama="aa")
        jumlah = Peserta.objects.filter(kegiatan=kegiatan_di_id).count()
        self.assertEquals(jumlah, 1)


class DataKegiatanUnitTests(TestCase):

    def test_data_kegiatan_url_is_exist(self):
        response = Client().get('/kegiatan/data/')
        self.assertEqual(response.status_code, 200)

    def test_data_kegiatan_using_template(self):
        response = Client().get('/kegiatan/data/')
        self.assertTemplateUsed(response,'keg-data.html')

    def test_activity_add_using_func(self):
        found = resolve('/kegiatan/data/')
        self.assertEqual(found.func, data)

    def test_model_create_peserta(self):
        new_kegiatan = Kegiatan.objects.create(kegiatan = 'ini kegiatan')
        Peserta.objects.create(kegiatan=new_kegiatan , nama="fyza")
        counting_all_available_kegiatan = Peserta.objects.all().count()
        self.assertEquals(counting_all_available_kegiatan,1)

    # def test_data_view_show(self):
    #     Kegiatan.objects.create(kegiatan='coba')
    #     request = HttpRequest()
    #     response = data(request)
    #     isi_html = response.content.decode('utf8')
    #     self.assertIn('coba', isi_html)

    def test_insert_blank_peserta(self):
        new_kegiatan = Kegiatan.objects.create(kegiatan = "test")
        form = PesertaForm(data = {'kegiatan' :new_kegiatan, 'nama' : ''})
        self.assertEqual(form.errors['nama'], ["This field is required."])

    def test_story6_delete_POST_request(self):
        Kegiatan.objects.create(kegiatan='keg')
        idpk = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=idpk)
        Peserta.objects.create(nama='fayza', kegiatan=kegiatan)
        response = Client().post('/kegiatan/data/', data={'delete-peserta':1})
        self.assertEqual(response.status_code,200)

    def test_story6_model_name(self):
        Kegiatan.objects.create(kegiatan='story6')
        idpk = Kegiatan.objects.all()[0].id
        kegiatan = Kegiatan.objects.get(id=idpk)
        Peserta.objects.create(nama='fayza', kegiatan=kegiatan)
        peserta = Peserta.objects.get(kegiatan=kegiatan)
        self.assertEquals(str(peserta),'fayza')


@tag('functional')
class IndexFunctionalTest(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()