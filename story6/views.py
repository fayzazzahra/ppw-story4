from django.shortcuts import redirect, render
from django.http import HttpResponse
from .models import Kegiatan, Peserta
from .forms import KegiatanForm, PesertaForm

# Create your views here.

def daftarkan(request):
    kegiatan = Kegiatan.objects.all()
    form = KegiatanForm()
    if request.method == 'POST':    
        forminput = KegiatanForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return render(request, 'keg-daftarkan.html', {'form':form, 'status': 'success', 'data' : kegiatan})
        else:
            return render(request, 'keg-daftarkan.html', {'form':form, 'status': 'failed', 'data' : kegiatan})       
    else:
        current_data = Kegiatan.objects.all()
        return render(request, 'keg-daftarkan.html', {'form':form, 'data':current_data})

def data(request):
    form = PesertaForm()
    kegiatan = Kegiatan.objects.all()
    peserta = Peserta.objects.all()
    if request.method == 'POST':
        forminput = PesertaForm(request.POST)
        if forminput.is_valid():
            forminput.save()
            return render(request,'keg-data.html', {'form':form, 'status': 'success', 'peserta' : peserta, 'kegiatan': kegiatan})
        else:
            peserta = Peserta.objects.all()
            return render(request, 'keg-data.html', {'form':form, 'status': 'failed', 'perserta' : peserta, 'kegiatan': kegiatan})
    else:
        return render(request,'keg-data.html', {'form':form, 'peserta' : peserta, 'kegiatan': kegiatan})