from django.urls import path
from story9.views import sign_up

app_name = 'story9'

urlpatterns = [
    path('signup', sign_up.as_view(), name='signup'),
]