from django.urls import path

from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('experiences', views.experiences, name='experiences'),
    path('contact', views.contact, name='contact'),
]
