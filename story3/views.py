from django.shortcuts import render

# Create your views here.

def profile(request):
    return render(request, 'story3/profile.html')\
    
def experiences(request):
    return render(request, 'story3/experiences.html')

def contact(request):
    return render(request, 'story3/contact.html')